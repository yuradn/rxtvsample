package com.example.yuri.rxsample;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.util.Arrays;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Observable;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";

    @Bind(R.id.fab)
    protected FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

    }

    @OnClick(R.id.fab)
    protected void clickToFab(View view){
        Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "onStart");
        //startFragmentView();
        startFragmentSample();
    }

    private void startFragmentView() {
        Fragment mFragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        if (mFragment == null) {
            Log.d(TAG, "New MainFragment created.");
            mFragment = new MainFragment();
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment_container, mFragment)
                    .commit();
        }
    }

    private void startFragmentSample() {
        Fragment mFragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        if (mFragment == null) {
            Log.d(TAG, "New MainFragment created.");
            mFragment = new RxSampleFragment();
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment_container, mFragment)
                    .commit();
        }
    }

    private void sampleListObrervable() {
        List<String> names = Arrays.asList("Didiet", "Doni", "Asep", "Reza",
                "Sari", "Rendi", "Akbar");
        Observable<String> helloObservable = Observable.from(names);

        helloObservable
                .map(String::toUpperCase)
                .subscribe(s -> {
                    String greet = "Sub 1 on " + currentThreadName() +
                            ": Hello " + s + "!";
                    System.out.println(greet);
                });
    }

    // Getting current thread name
    public static String currentThreadName() {
        return Thread.currentThread().getName();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
