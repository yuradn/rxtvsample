package com.example.yuri.rxsample;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.io.Serializable;
import java.util.concurrent.TimeUnit;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by test on 4/27/16.
 */
public class RxSampleFragment extends Fragment {
    private static final String TAG = "RxSample";

    @Bind(R.id.tvLog)
    protected TextView tvLog;
    @Bind(R.id.tvNext)
    protected TextView tvNext;

    private Subscription mInterval;
    long mTime;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView");
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_sample, container, false);
        ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        String[] mArray = new String[]{"first", "second", "", "third", "fifth"};

        mTime = System.currentTimeMillis();

        mInterval =
        Observable.just(3)
                .interval(3, TimeUnit.SECONDS)
                .subscribe(new Subscriber<Long>() {
                    int iteration=0;

                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(Long aLong) {
                        Log.d(TAG, "Iteration: "+iteration++);
                    }
                });

        Observable.from(mArray)
                .map(s -> {
                    /*try {
                        Thread.sleep(TimeUnit.SECONDS.toMillis(3));
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }*/
                    for (int i = 0; i < 900000000; i++) {
                        int a = 121331;
                        int b = 23232;
                        int c;
                        c = a / b * a;
                    }
                    Log.d(TAG, "Thread id: " + Thread.currentThread().getId());
                    return s;
                })
                /*.timeout(1, TimeUnit.SECONDS)
                //.retry(3)
                .onErrorReturn(new Func1<Throwable, String>() {
                    @Override
                    public String call(Throwable throwable) {
                        Log.d(TAG, "Thread id: "+Thread.currentThread().getId());
                        return "blabla";
                    }
                })*/
                .map(new Func1<String, String>() {
                    @Override
                    public String call(String s) {
                        Log.d(TAG, "Thread id: " + Thread.currentThread().getId());
                        return s.trim();
                    }

                })
                //.skip(2)
                .filter(s -> !TextUtils.isEmpty(s))
                .map(s -> {
                    Log.d(TAG, "Thread id: " + Thread.currentThread().getId());
                    s = s + " " + (System.currentTimeMillis() - mTime);
                    mTime = System.currentTimeMillis();
                    return s;
                })
                .map(s -> {
                    Log.d(TAG, "Thread id: " + Thread.currentThread().getId());
                    return s + "\n";
                })
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<String>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG, "Thread id: " + Thread.currentThread().getId());
                        tvNext.append(e.getMessage() + "\n");
                    }

                    @Override
                    public void onNext(String s) {
                        Log.d(TAG, "Thread id: " + Thread.currentThread().getId());
                        tvNext.append(s);
                    }
                });
    }

    private void obs5() {
        Observable.just("Hello, world", 2016, "New!")
                .map(new Func1<Serializable, Object>() {
                    @Override
                    public Object call(Serializable serializable) {
                        Log.d(TAG, "Serializable: " + serializable.getClass().getName());
                        if (serializable instanceof String) {
                            serializable = serializable + "+";
                        }
                        try {
                            Thread.sleep(TimeUnit.SECONDS.toMillis(5));
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        return serializable;
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Object>() {
                    @Override
                    public void call(Object o) {
                        Log.d(TAG, "Object: " + o.getClass());
                        tvLog.append(o.getClass().getName() + ": " + o + "\n");
                    }
                });
    }

    private void thirdObs() {
        Observable.just("Hello, world!")
                .map(s -> s + " -Yuri")
                .map(s -> s.hashCode())
                .map(i -> Integer.toString(i))
                .subscribe(s -> tvNext.append(s));
    }

    private void secondObs() {
        Observable<String> myObservable = Observable.just("Hello World!");

        Action1<String> onNextAction = new Action1<String>() {
            @Override
            public void call(String s) {
                Log.d(TAG, "onNext: " + s);
                tvNext.append(s);
            }
        };

        myObservable.subscribe(onNextAction);
    }

    private void firstObs() {
        Observable<String> myObservable = Observable.create(
                new Observable.OnSubscribe<String>() {
                    @Override
                    public void call(Subscriber<? super String> subscriber) {
                        subscriber.onNext("Hello World!");
                        subscriber.onCompleted();
                    }
                }
        );

        Subscriber<String> mySubscriber = new Subscriber<String>() {
            @Override
            public void onCompleted() {
                Log.d(TAG, "onCompleted");
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
            }

            @Override
            public void onNext(String s) {
                Log.d(TAG, "onNext: " + s);
                tvNext.append(s);
            }
        };

        myObservable.subscribe(mySubscriber);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d(TAG, "onDestroyView");
        ButterKnife.unbind(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mInterval!=null) {
            mInterval.unsubscribe();
        }
    }
}
