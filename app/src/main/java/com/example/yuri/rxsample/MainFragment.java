package com.example.yuri.rxsample;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.jakewharton.rxbinding.widget.RxTextView;
import com.jakewharton.rxbinding.widget.TextViewTextChangeEvent;

import java.util.regex.Pattern;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.Observable;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by test on 4/4/16.
 */
public class MainFragment extends Fragment {

    private static final String TAG = "MainFragment";
    @Bind(R.id.edtName)
    protected EditText edtName;
    @Bind(R.id.edtEmail)
    protected EditText edtEmail;
    @Bind(R.id.btnRegister)
    protected Button btnRegister;

    // instantiate composite subscription
    private CompositeSubscription compositeSubs = new CompositeSubscription();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView");
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "onStart");

        Observable<TextViewTextChangeEvent> userNameText = RxTextView.textChangeEvents(edtName);

        userNameText.filter(t -> t.text().length() > 4)
                .subscribe(t -> {
                    Log.d(TAG, t.text().toString()
                            + " start: " + t.start()
                            + " before: " + t.before()
                            + " count: " + t.count());
                });

        Pattern emailPattern = Pattern.compile(
                "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");

        Observable<Boolean> userNameValid = RxTextView.textChangeEvents(edtName) //  [2]
                .map(TextViewTextChangeEvent::text)
                .map(t -> t.length() > 4);

        Observable<Boolean> emailValid = RxTextView.textChangeEvents(edtEmail)
                .map(TextViewTextChangeEvent::text)
                .map(t -> emailPattern.matcher(t).matches());

        emailValid.map(b -> b ? Color.BLACK : Color.RED)
                .subscribe(edtEmail::setTextColor);

        userNameValid.map( b -> b ? Color.BLACK : Color.RED)
                .subscribe(edtName::setTextColor);

        Observable<Boolean> registerEnabled =
                Observable.combineLatest(userNameValid, emailValid, (a,b) -> a && b);
        registerEnabled.subscribe(btnRegister::setEnabled);

        compositeSubs.add(
                emailValid.distinctUntilChanged().doOnNext(b -> Log.d("[Rx]", "Email " + (b ? "Valid" : "Invalid")))
                        .map(b -> b ? Color.BLACK : Color.RED)
                        .subscribe(edtEmail::setTextColor));

        compositeSubs.add(
                userNameValid.distinctUntilChanged().doOnNext(b -> Log.d("[Rx]", "Uname " + (b ? "Valid" : "Invalid")))
                        .map(b -> b ? Color.BLACK : Color.RED)
                        .subscribe(edtName::setTextColor));

// and the registerenabled

        registerEnabled.distinctUntilChanged().doOnNext(b -> Log.d("[Rx]", "Button " + (b ? "Enabled" : "Disabled")))
                .subscribe(btnRegister::setEnabled);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d(TAG, "onDestroyView");
        compositeSubs.unsubscribe();
        ButterKnife.unbind(this);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.d(TAG, "onAttach");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d(TAG, "onDetach");
    }

}
